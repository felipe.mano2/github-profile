document.addEventListener('DOMContentLoaded', (event) => {
    const usersUrl = "https://api.github.com/users/"
    const repos = "/repos?per_page=100"
    const repositorios = document.getElementById('rep')

    document.getElementById('pesquisar').addEventListener('click', () => {
        repositorios.textContent = ""
        fetch(usersUrl + user.value)
            .then((res) => res.json())
            .then((data) => {
                document.getElementById("foto").setAttribute("src", data.avatar_url)
                document.getElementById('dados').style.display = "block"
                document.getElementsByClassName('rep')[0].style.display = "flex"
                document.getElementById('name').textContent = data.name
                document.getElementById('name').style.display = "flex"
                document.getElementById('repdata').textContent = data.public_repos
            })
            .catch((err) => console.log(err))

        fetch(usersUrl + user.value + repos)
            .then((res) => res.json())
            .then((data) => {
                data.forEach((rep) => {
                    const li = document.createElement('li')
                    li.textContent = rep.name
                    repositorios.append(li)
                    console.log(rep.name)
                })
            })
    })
})
